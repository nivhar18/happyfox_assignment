package helpdeskutility.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions
(
	features="src/test/java/helpdeskutility/features/helpdeskticket.feature",
	glue= {"helpdeskutility/stepdefinition"},monochrome=true,
	tags= {"@AgentLogin,@TicketCreation,@DeleteStatusPriority"},
	//@AgentLogin,@TicketCreation,@DeleteStatusPriority
	plugin = { "html:target/cucumber-reports/cucu-html-report",
		"json:target/cucumber-reports/cucejson.json",
		"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"}
)

public class RunTest {
}

