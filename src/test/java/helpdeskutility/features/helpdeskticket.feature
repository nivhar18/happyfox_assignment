@HelpdeskTicket
Feature: Helpdesk ticket workflow of Happyfox application
#Background:
#Given Open the chrome Browser 
#Then maximize the browser
#And Set the TimeOuts also Hit the URL

@AgentLogin
Scenario: Login to agent portal then create status and priority 
When Agent enters the username and password 
And Agent creates a new status and priority

@TicketCreation
Scenario: User creates a new ticket where status and priority is verified followed reply using canned actions
When Customer creates a new ticket on support center with attachment
And Agent verifies the default status and priority
Then Agent replies to customer using canned actions and checks the property changes of status and priority

@DeleteStatusPriority
Scenario: Agent deletes the status and priority then logs out of the portal
When Agent deletes the created status and priority
Then Agent logs out of the portal




