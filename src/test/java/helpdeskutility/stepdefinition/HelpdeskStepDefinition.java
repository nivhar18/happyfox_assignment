package helpdeskutility.stepdefinition;

import org.junit.After;
import org.junit.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpdeskutility.actionlib.ActionLib;
import helpdeskutility.pages.AgentLogin;
import helpdeskutility.pages.DeletePrioStatus;
import helpdeskutility.pages.TicketCreation;

public class HelpdeskStepDefinition extends ActionLib{

	ActionLib actionLib=new ActionLib();
	@Before
	public void intializeTest()
	{		
		//actionLib.initialize();
	}

	@After
	public void endTest()
	{
		actionLib.closeAllBrowsers();
	}

	//SC-01
	AgentLogin agententer =new AgentLogin(driver);
	@When("^Agent enters the username and password$")
	public void agent_enters_the_username_and_password() throws Throwable {		
		agententer.enterUsername();
		agententer.enterPassword();
		agententer.loginBtnClick();
	}

	@And("^Agent creates a new status and priority$")
	public void agent_creates_a_new_status_and_priority() throws Throwable {
		agententer.TicketmenuHover();
		agententer.statusClick();
		agententer.createStatus();
		agententer.createPriority();
	}

	//SC-02
	TicketCreation ticket=new TicketCreation(driver);
	@When("^Customer creates a new ticket on support center with attachment$")
	public void customer_creates_a_new_ticket_on_support_center_with_attachment() throws Throwable {
		ticket.makeasDefaultPriority();
		ticket.makeasDefaultStatus();
		ticket.ticketCreation();	
	}

	@And("^Agent verifies the default status and priority$")
	public void agent_verifies_the_default_status_and_priority() throws Throwable {
		ticket.verifyTicket();
	}

	@Then("^Agent replies to customer using canned actions and checks the property changes of status and priority$")
	public void agent_replies_to_customer_using_canned_actions_and_checks_the_property_changes_of_status_and_priority() throws Throwable {		
		ticket.deletePriorityMenu();
	}

	//SC-03
	DeletePrioStatus deletestate = new DeletePrioStatus(driver);
	@When("^Agent deletes the created status and priority$")
	public void agent_deletes_the_created_status_and_priority() throws Throwable {
		deletestate.deleteExistingPriority();
		deletestate.deleteExistingStatus();
		deletestate.agentLogout();
	}

	@Then("^Agent logs out of the portal$")
	public void agent_logs_out_of_the_portal() throws Throwable {
		deletestate.closeAllBrowsers();
	}


}
