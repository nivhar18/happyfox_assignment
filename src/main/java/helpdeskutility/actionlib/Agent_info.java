package helpdeskutility.actionlib;


public class Agent_info {
	public static String agentName="interview_agent";
	public static String agentPwd="Interview@123";
	public static String portalUrl="https://interview.supporthive.com/staff";
	public static String supportcenterUrl="https://interview.supporthive.com/new/";
	public static String ticketUrl="https://interview.supporthive.com/staff/tickets";
	public static String statusNameText="Issue created";
	public static String statusDescText="Status when a new issue ticket is created in HappyFox";
	public static String priorityNameText="Assistance required";
	public static String priorityDescText="Priority of the newly created tickets";
}
