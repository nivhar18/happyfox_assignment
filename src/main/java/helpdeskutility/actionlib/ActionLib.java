package helpdeskutility.actionlib;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;


public class ActionLib {
	public static WebDriver driver;
	static int i=1;	
		//browserVersion: 84.0
	
	public void initialize()
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");				
		driver=new ChromeDriver();
		PageFactory.initElements(driver, this); 
	}
	public void startApp(String browser, String url) {
		try {
			initialize();
			driver.get(url);		
			driver.manage().window().maximize();			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			System.out.println("The browser has launched successfully");
		} 
		catch (WebDriverException e){			
			PrintLoggerInfo("The browser could not be launched");
		}
	}

	public static void ExplicitWaitTime(long ms) throws InterruptedException
	{
		Thread.sleep(ms);
	}

	public static void WaitWebdriverTimeClickable(long sec,String xpathExpression) throws InterruptedException
	{
		WebDriverWait waitclick = new WebDriverWait(driver, sec);
		waitclick.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathExpression))); 
	}

	public static void WaitWebdriverTimeVisible(long sec,String xpathExpression) throws InterruptedException
	{
		WebDriverWait waitvisible = new WebDriverWait(driver, sec);
		waitvisible.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathExpression))); 
	}

	public static void PrintLoggerInfo(String msg)
	{
		System.out.println(msg);
	}

	public static void ActionBuilderClickandHold(WebElement name)
	{
		Actions builder = new Actions(driver);
		builder.moveToElement(name).clickAndHold().perform();
	}

	public void ActionBuilderClick(WebElement name)
	{
		Actions builder = new Actions(driver);
		builder.moveToElement(name).click().perform();
	}

	public void ActionBuilderDoubleClick(WebElement name)
	{
		Actions builder = new Actions(driver);
		builder.moveToElement(name).doubleClick().perform();
	}

	public void ActionBuilderMovetoElement(WebElement name)
	{
		Actions builder = new Actions(driver);
		builder.moveToElement(name).perform();
	}

	public void isUnderlined(WebElement name,String msg)
	{
		if(name.getCssValue("text-decoration").contains("underline"))
		{
			PrintLoggerInfo("The "+msg+" link is underlined");
		}
		else
		{
			Assert.fail("The "+msg+" link is not underlined");
		}
	}

	public static void type(WebElement ele, String data) {

		ele.sendKeys(data);
		String msg="The data entered successfully";
		if(ele.isDisplayed()==true)
		{
			PrintLoggerInfo(msg);
		}
		else
		{
			Assert.fail("The data could not be entered");
		}
	}

	public static void click(WebElement ele) {
		try {
			ele.click();
			PrintLoggerInfo("The element : "+ele.getText()+" is clicked successfully");

		}
		catch(Exception e)
		{
			//PrintLoggerInfo("The element: "+ele.getText()+" could not clicked");				
		}
	}

	public void scrollDownBarBottom()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;		
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void switchWindow(int index1,int index2)
	{
		Set<String> allwin = driver.getWindowHandles();//has all window references
		List<String> listwin=new ArrayList<>();
		listwin.addAll(allwin);
		driver.switchTo().window(listwin.get(index1));
		PrintLoggerInfo("Title: "+driver.getTitle()+"\n"+"URL: "+driver.getCurrentUrl());
		driver.switchTo().window(listwin.get(index2));
	}

	public void clearText(WebElement ele)
	{
		ele.clear();
		PrintLoggerInfo("The entered text is cleared" );
	}

	public String getTextElement(WebElement ele) {	
		String getTextReturn = null;
		try {
			getTextReturn = ele.getText();
		} catch (WebDriverException e) {
			PrintLoggerInfo("WebDriverException"+e.getMessage());
		}
		return getTextReturn;
	}

	public String getAttribute(WebElement ele, String attribute) {		
		String attrReturn;

		attrReturn=  ele.getAttribute(attribute);
		if(attrReturn!=null)
		{
			PrintLoggerInfo("The attribute text: "+ele.getAttribute(attribute));
		}
		else
		{
			Assert.fail("Unable to get the text of element: "+ele.getAttribute(attribute));
		}

		return attrReturn;
	}

	public boolean verifyExactText(WebElement ele, String expectedText) {

		if(getTextElement(ele).equals(expectedText)) {
			PrintLoggerInfo("The expected text is matched "+expectedText);
			return true;
		}
		else {
			Assert.fail("The expected text match has failed "+expectedText);
			return false;
		}
	}

	public boolean getCssValue(WebElement ele, String value) {

		if(ele.getCssValue(value) != null) {
			PrintLoggerInfo("The expected text is matching "+value);
			return true;
		}
		else {
			Assert.fail("The expected text match has failed "+value);
			return false;
		}
	}

	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(getTextElement(ele).contains(expectedText)) {
				PrintLoggerInfo("The expected text contains the actual "+expectedText);
			}else {
				Assert.fail("The expected text doesn't contain the actual "+expectedText);
			}
		} catch (WebDriverException e) {
			PrintLoggerInfo("WebDriverException : "+e.getMessage());
		} 
	}

	public boolean verifyDisplayed(WebElement ele) {

		if(ele.isDisplayed()) {
			PrintLoggerInfo("The element "+ele+" is visible");
			return true;
		} else {
			Assert.fail("The element "+ele+" is not visible");
			return false;
		}
	}

	public static void takeSnap(){	  
		try { FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE) , new File("./Screenshots/snapshot_"+i+".jpg")); } 
		catch (WebDriverException e) {
			PrintLoggerInfo("The browser has been closed."); } 
		catch (IOException e) {
			PrintLoggerInfo("The snapshot could not be taken"); }
		i++;
	}

	public void goBackButton() 
	{
		driver.navigate().back();
	}

	public void closeBrowser() {
		try {
			driver.close();
			PrintLoggerInfo("The browser is closed");
		} catch (Exception e) {
			PrintLoggerInfo("The browser could not be closed");
		}
	}

	public void closeAllBrowsers() {
		try {
			driver.quit();
			PrintLoggerInfo("The opened browsers are closed");
		} catch (Exception e) {
			PrintLoggerInfo("The browsers could not be closed");
		}
	}
}

