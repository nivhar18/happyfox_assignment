package helpdeskutility.pages;


import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helpdeskutility.actionlib.Agent_info;
import helpdeskutility.actionlib.ActionLib;

public class DeletePrioStatus extends ActionLib{
	WebDriver driver=ActionLib.driver;
	public DeletePrioStatus(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH,using="//div[@id='ember32']")
	WebElement ticketMenu;

	@FindBy(how=How.XPATH,using="//span[text()='Assistance required']")
	WebElement prioname;

	@FindBy(how=How.XPATH,using="//div[text()='Issue created']")
	WebElement statusname;

	@FindBy(how=How.XPATH,using="//div[2]/div/div[1]/div[2]/div/ul/li[2]/a")
	WebElement statusLink;

	@FindBy(how=How.XPATH,using="//div[2]/div/table/tbody/tr[5]/td[5]")
	WebElement statusHover;

	@FindBy(how=How.XPATH,using="//a[text()='Delete']")
	WebElement deleteBtn;

	@FindBy(how=How.XPATH,using="//span[text()='Choose Status']")
	WebElement selectchangeStatus;

	@FindBy(how=How.XPATH,using="//span[text()='Choose Priority']")
	WebElement selectchangePrio;

	@FindBy(how=How.XPATH,using="//li[text()='New']")
	WebElement selectnewstatus;

	@FindBy(how=How.XPATH,using="//li[text()='Medium']")
	WebElement selectnewPrio;

	@FindBy(how=How.XPATH,using="//button[@data-test-id='delete-dependants-primary-action']")
	WebElement hardDelete;

	@FindBy(how=How.XPATH,using="//div[2]/nav/div[7]/div/div[1]")
	WebElement LogoutLogo;

	@FindBy(how=How.XPATH,using="//a[text()='Logout']")
	WebElement LogoutBtn;

	public void deleteExistingPriority() throws InterruptedException
	{	
		click(prioname);
		takeSnap();
		ActionBuilderClick(deleteBtn);
		ExplicitWaitTime(2000);
		takeSnap();
		try {
			selectchangePrio.isDisplayed();
			ActionBuilderClick(selectchangePrio);
			ActionBuilderClick(selectnewPrio);
			takeSnap();
		}
		catch (NoSuchElementException e) {
			System.out.println("Change default priority drop down is not present");
		}
		finally
		{
			ActionBuilderClick(hardDelete);
			ExplicitWaitTime(1000);
			takeSnap();	
		}
	}
	public void deleteExistingStatus() throws InterruptedException
	{	
		ActionBuilderMovetoElement(ticketMenu);
		ExplicitWaitTime(3000);
		ActionBuilderClickandHold(statusLink);	
		takeSnap();
		click(statusLink);
		click(statusname);
		takeSnap();
		ActionBuilderClick(deleteBtn);
		ExplicitWaitTime(1000);
		takeSnap();
		try
		{
			selectchangeStatus.isDisplayed();
			ActionBuilderClick(selectchangeStatus);
			ActionBuilderClick(selectnewstatus);
			takeSnap();
		}
		catch (NoSuchElementException e) {
			System.out.println("Change default status drop down is not present");
		}
		finally
		{
			ActionBuilderClick(hardDelete);
			ExplicitWaitTime(1000);
			takeSnap();	
		}
	}	

	public void agentLogout() throws InterruptedException
	{
		WaitWebdriverTimeClickable(5,"//div[2]/nav/div[7]/div/div[1]");
		//ActionBuilderClickandHold(LogoutLogo);
		//ExplicitWaitTime(2000);	
		ActionBuilderMovetoElement(LogoutLogo);
		ActionBuilderClick(LogoutLogo);
		ExplicitWaitTime(2000);
		takeSnap();
		click(LogoutBtn);
		ExplicitWaitTime(2000);
		takeSnap();
	}
}