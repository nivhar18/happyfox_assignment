package helpdeskutility.pages;


import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helpdeskutility.actionlib.Agent_info;
import helpdeskutility.actionlib.ActionLib;

public class AgentLogin extends ActionLib{
	WebDriver driver=ActionLib.driver;
	public AgentLogin(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH,using="//input[@id='id_username']")
	WebElement username;

	@FindBy(how=How.XPATH,using="//input[@id='id_password']")
	WebElement password;

	@FindBy(how=How.XPATH,using="//input[@type='submit']")
	WebElement loginBtn;

	@FindBy(how=How.XPATH,using="//div[@id='ember32']")
	WebElement ticketMenu;

	@FindBy(how=How.XPATH,using="//div[2]/div/div[1]/div[2]/div/ul/li[2]/a")
	WebElement statusLink;

	@FindBy(how=How.XPATH,using="//button[@class='hf-mod-create']")
	WebElement addIcon;

	@FindBy(how=How.XPATH,using="//input[@class='hf-form-field-input hf-text-input   ember-text-field ember-view']")
	WebElement nameField;

	@FindBy(how=How.XPATH,using="//textarea[@aria-label='Description']")
	WebElement DescField;

	@FindBy(how=How.XPATH,using="//button[@data-test-id='add-status']")
	WebElement addStatusBtn;

	@FindBy(how=How.XPATH,using="//div[2]/div/div[1]/div[2]/div/ul/li[3]/a")
	WebElement priorityLink;

	@FindBy(how=How.XPATH,using="//button[@data-test-id='add-priority']")
	WebElement addPriorityBtn;

	@FindBy(how=How.XPATH,using="//span[text()='Assistance required']")
	WebElement prioname;

	@FindBy(how=How.XPATH,using="//div[text()='Issue created']")
	WebElement statusname;

	@FindBy(how=How.XPATH,using="//div[2]/div/table/tbody/tr[5]/td[5]")
	WebElement prioHover;

	@FindBy(how=How.XPATH,using="(//div[text()='Make Default'])[4]")
	WebElement setdefaultpriority;

	@FindBy(how=How.XPATH,using="//div[2]/div/table/tbody/tr[5]/td[5]")
	WebElement statusHover;

	@FindBy(how=How.XPATH,using="(//a[text()='Make Default'])[4]")
	WebElement setdefaultstatus;

	@FindBy(how=How.XPATH,using="//a[text()='Delete']")
	WebElement deleteBtn;

	@FindBy(how=How.XPATH,using="//span[text()='Choose Status']")
	WebElement selectchangeStatus;

	@FindBy(how=How.XPATH,using="//span[text()='Choose Priority']")
	WebElement selectchangePrio;

	@FindBy(how=How.XPATH,using="//li[text()='New']")
	WebElement selectnewstatus;

	@FindBy(how=How.XPATH,using="//li[text()='Medium']")
	WebElement selectnewPrio;

	@FindBy(how=How.XPATH,using="//button[@data-test-id='delete-dependants-primary-action']")
	WebElement hardDelete;

	@FindBy(how=How.XPATH,using="//div[@data-test-id='staff-menu']")
	WebElement LogoutLogo;

	@FindBy(how=How.XPATH,using="//a[text()='Logout']")
	WebElement LogoutBtn;

	public void enterUsername() throws InterruptedException
	{		
		startApp("chrome", Agent_info.portalUrl);	
		ExplicitWaitTime(2000);
		type(username,Agent_info.agentName);	
		takeSnap();
	}
	public void enterPassword()
	{
		type(password,Agent_info.agentPwd);	
		takeSnap();
	}
	public void loginBtnClick() throws InterruptedException
	{
		click(loginBtn);
		takeSnap();
		ExplicitWaitTime(5000);	
	}	
	public void TicketmenuHover() throws InterruptedException
	{					
		ActionBuilderMovetoElement(ticketMenu);
		ExplicitWaitTime(3000);
		takeSnap();
	}
	public void statusClick() throws InterruptedException
	{
		//WaitWebdriverTimeVisible(15,"//div[@class='hf-module-switcher-module']/following::a[@class='active ember-view']");
		ExplicitWaitTime(5000);
		ActionBuilderClickandHold(statusLink);	
		takeSnap();
		click(statusLink);
	}
	public void textFieldFill() throws InterruptedException {
		//reusable method for status and priority
		ExplicitWaitTime(2000);
		takeSnap();
		click(addIcon);
		takeSnap();

	}
	public void createStatus() throws InterruptedException
	{	
		textFieldFill();
		type(nameField,Agent_info.statusNameText);
		type(DescField,Agent_info.statusDescText);
		takeSnap();
		click(addStatusBtn);
		ExplicitWaitTime(2000);
		takeSnap();

	}
	public void createPriority() throws InterruptedException
	{
		ActionBuilderMovetoElement(ticketMenu);
		ExplicitWaitTime(3000);
		ActionBuilderClickandHold(priorityLink);	
		takeSnap();
		click(priorityLink);
		textFieldFill();
		type(nameField,Agent_info.priorityNameText);
		type(DescField,Agent_info.priorityDescText);
		takeSnap();
		click(addPriorityBtn);
		ExplicitWaitTime(2000);
		takeSnap();
	}
	
}