package helpdeskutility.pages;


import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import helpdeskutility.actionlib.Agent_info;
import helpdeskutility.actionlib.ActionLib;


public class TicketCreation extends ActionLib{
	WebDriver driver=ActionLib.driver;
	public TicketCreation(WebDriver driver) 
	{
		this.driver=driver;
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how=How.XPATH,using="//div[2]/div/div[1]/div[2]/div/ul/li[3]/a")
	WebElement priorityLink;

	@FindBy(how=How.XPATH,using="//div[2]/div/div[1]/div[2]/div/ul/li[2]/a")
	WebElement statusLink;

	@FindBy(how=How.XPATH,using="//span[text()='Assistance required']")
	WebElement prioname;

	@FindBy(how=How.XPATH,using="//div[text()='Issue created']")
	WebElement statusname;

	@FindBy(how=How.XPATH,using="//div[2]/div/table/tbody/tr[5]/td[5]")
	WebElement prioHover;

	@FindBy(how=How.XPATH,using="(//div[text()='Make Default'])[4]")
	WebElement setdefaultpriority;

	@FindBy(how=How.XPATH,using="//div[2]/div/table/tbody/tr[5]/td[5]")
	WebElement statusHover;

	@FindBy(how=How.XPATH,using="(//a[text()='Make Default'])[4]")
	WebElement setdefaultstatus;

	@FindBy(how=How.XPATH,using="//span[text()='Choose Status']")
	WebElement selectchangeStatus;

	@FindBy(how=How.XPATH,using="//span[text()='Choose Priority']")
	WebElement selectchangePrio;

	@FindBy(how=How.XPATH,using="//li[text()='New']")
	WebElement selectnewstatus;

	@FindBy(how=How.XPATH,using="//li[text()='Medium']")
	WebElement selectnewPrio;

	@FindBy(how=How.XPATH,using="//div[@id='ember32']")
	WebElement ticketMenu;

	@FindBy(how=How.XPATH,using="//input[@id='id_subject']")
	WebElement subject;

	@FindBy(how=How.XPATH,using="//div[1]/div[2]/div/div/div/div")
	WebElement msgbox;

	@FindBy(how=How.XPATH,using="//a[text()='Browse for a file']")
	WebElement browseFile;

	@FindBy(how=How.XPATH,using="//input[@id='id_name']")
	WebElement name;

	@FindBy(how=How.XPATH,using="//input[@id='id_email']")
	WebElement email;

	@FindBy(how=How.XPATH,using="//button[text()='Create Ticket']")
	WebElement createTicketBtn;

	@FindBy(how=How.XPATH,using="(//a[@title='Issue in contact page'])[1]")
	WebElement ticketSelect;

	@FindBy(how=How.XPATH,using="//span[text()='Assistance required']")
	WebElement ticketPriority;

	@FindBy(how=How.XPATH,using="(//span[text()='Issue created'])[2]")
	WebElement ticketStatus;

	@FindBy(how=How.XPATH,using="//a[@data-test-id='reply-link']")
	WebElement reply;

	@FindBy(how=How.XPATH,using="//span[@class='hf-u-vertical-super ']")
	WebElement cannedAction;

	@FindBy(how=How.XPATH,using="(//li[@class='ember-power-select-option'])[1]")
	WebElement chooseCannedAction;

	@FindBy(how=How.XPATH,using="//button[text()='Apply']")
	WebElement apply;

	@FindBy(how=How.XPATH,using="//button[@data-test-id='add-update-reply-button']")
	WebElement addReply;

	@FindBy(how=How.XPATH,using="//span[text()='Medium']")
	WebElement updatedPrio;

	@FindBy(how=How.XPATH,using="//article[1]/ul/li[2]/span[2]")
	WebElement updatedStatus;

	@FindBy(how=How.XPATH,using="//article[1]/ul/li[3]/span")
	WebElement updatedTag;


	public void makeasDefaultPriority() throws InterruptedException
	{
		if(verifyExactText(prioname,"Assistance required"))
		{
			PrintLoggerInfo("The priority 'Assistance required' exists");
			ActionBuilderMovetoElement(prioHover);
			takeSnap();
			ExplicitWaitTime(2000);
			ActionBuilderClick(setdefaultpriority);
			ExplicitWaitTime(2000);
			takeSnap();
		}
		else 
		{
			Assert.fail("The priority 'Assistance required' doesn't exist.Invalid priority!!");
		}	
	}
	public void statusClick() throws InterruptedException
	{
		ExplicitWaitTime(2000);
		ActionBuilderClickandHold(statusLink);	
		takeSnap();
		click(statusLink);
	}	
	public void TicketmenuHover() throws InterruptedException
	{					
		ActionBuilderMovetoElement(ticketMenu);
		ExplicitWaitTime(1000);
		takeSnap();
	}
	public void makeasDefaultStatus() throws InterruptedException
	{	
		TicketmenuHover();
		statusClick();
		if(verifyExactText(statusname,"ISSUE CREATED"))
		{
			PrintLoggerInfo("The Status 'Issue created' exists");
			ActionBuilderMovetoElement(statusHover);
			takeSnap();
			ExplicitWaitTime(2000);
			ActionBuilderClick(setdefaultstatus);
			ExplicitWaitTime(2000);
			takeSnap();
		}
		else 
		{
			Assert.fail("The status 'Issue created' doesn't exist.Invalid status!!");
		}	
	}

	public void ticketCreation() throws InterruptedException
	{
		driver.navigate().to(Agent_info.supportcenterUrl);
		ExplicitWaitTime(2000);
		type(subject, "Issue in contact page");
		type(msgbox,"Having issue in contact page through mail");
		takeSnap();
		scrollDownBarBottom();
		//ExplicitWaitTime(2000);
		//browseFile.sendKeys("./upload/Ticket_snapshot.png");			
		ExplicitWaitTime(2000);
		takeSnap();
		type(name,"Nivedha");
		type(email,"xyz@gmail.com");
		click(createTicketBtn);
		ExplicitWaitTime(3000);
		takeSnap();
	}
	public void verifyTicket() throws InterruptedException
	{
		driver.navigate().to(Agent_info.ticketUrl);
		ExplicitWaitTime(2000);
		takeSnap();
		ActionBuilderClick(ticketSelect);
		ExplicitWaitTime(4000);
		scrollDownBarBottom();
		takeSnap();
		if(verifyExactText(ticketPriority,"Assistance required"))
		{
			PrintLoggerInfo("The priority 'Assistance required' matched with default priority");
		}
		else 
		{
			Assert.fail("The priority 'Assistance required' doesn't exist.Invalid priority!!");
		}
		if(verifyExactText(ticketStatus,"ISSUE CREATED"))
		{
			PrintLoggerInfo("The Status 'Issue created' matched with default status");
		}
		else 
		{
			Assert.fail("The status 'Issue created' doesn't exist.Invalid status!!");
		}	
		ExplicitWaitTime(2000);
		ActionBuilderMovetoElement(reply);
		click(reply);
		ExplicitWaitTime(2000);
		ActionBuilderMovetoElement(cannedAction);
		ActionBuilderClick(cannedAction);
		ExplicitWaitTime(2000);
		takeSnap();
		ActionBuilderClick(chooseCannedAction);
		ExplicitWaitTime(2000);
		takeSnap();
		click(apply);
		ExplicitWaitTime(2000);
		takeSnap();
		click(addReply);
		ExplicitWaitTime(5000);
		takeSnap();
		if((verifyExactText(updatedPrio,"Medium") && verifyExactText(updatedStatus,"CLOSED") && verifyExactText(updatedTag,"customer_query"))==true)
		{
			System.out.println("Ticket property changes for status,priority and tags are valid");
		}
		else
		{
			Assert.fail("Ticket property changes for status,priority and tags are valid");
		}
		
	}

	public void deletePriorityMenu() throws InterruptedException
	{	
		ExplicitWaitTime(3000);
		ActionBuilderMovetoElement(ticketMenu);
		ExplicitWaitTime(3000);
		ActionBuilderClickandHold(priorityLink);	
		takeSnap();
		click(priorityLink);
	}
}