$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/helpdeskutility/features/helpdeskticket.feature");
formatter.feature({
  "line": 2,
  "name": "Helpdesk ticket workflow of Happyfox application",
  "description": "",
  "id": "helpdesk-ticket-workflow-of-happyfox-application",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@HelpdeskTicket"
    }
  ]
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "#Background:"
    },
    {
      "line": 4,
      "value": "#Given Open the chrome Browser"
    },
    {
      "line": 5,
      "value": "#Then maximize the browser"
    },
    {
      "line": 6,
      "value": "#And Set the TimeOuts also Hit the URL"
    }
  ],
  "line": 9,
  "name": "Login to agent portal then create status and priority",
  "description": "",
  "id": "helpdesk-ticket-workflow-of-happyfox-application;login-to-agent-portal-then-create-status-and-priority",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@AgentLogin"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "Agent enters the username and password",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Agent creates a new status and priority",
  "keyword": "And "
});
formatter.match({
  "location": "HelpdeskStepDefinition.agent_enters_the_username_and_password()"
});
formatter.result({
  "duration": 33076052500,
  "status": "passed"
});
formatter.match({
  "location": "HelpdeskStepDefinition.agent_creates_a_new_status_and_priority()"
});
formatter.result({
  "duration": 33636726900,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "User creates a new ticket where status and priority is verified followed reply using canned actions",
  "description": "",
  "id": "helpdesk-ticket-workflow-of-happyfox-application;user-creates-a-new-ticket-where-status-and-priority-is-verified-followed-reply-using-canned-actions",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 13,
      "name": "@TicketCreation"
    }
  ]
});
formatter.step({
  "line": 15,
  "name": "Customer creates a new ticket on support center with attachment",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "Agent verifies the default status and priority",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Agent replies to customer using canned actions and checks the property changes of status and priority",
  "keyword": "Then "
});
formatter.match({
  "location": "HelpdeskStepDefinition.customer_creates_a_new_ticket_on_support_center_with_attachment()"
});
formatter.result({
  "duration": 22988878900,
  "error_message": "org.openqa.selenium.ElementNotInteractableException: element not interactable\n  (Session info: chrome\u003d84.0.4147.89)\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027HP-PC\u0027, ip: \u0027192.168.0.106\u0027, os.name: \u0027Windows 8.1\u0027, os.arch: \u0027x86\u0027, os.version: \u00276.3\u0027, java.version: \u00271.8.0\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 84.0.4147.89, chrome: {chromedriverVersion: 84.0.4147.30 (48b3e868b4cc0..., userDataDir: C:\\Users\\HP\\AppData\\Local\\T...}, goog:chromeOptions: {debuggerAddress: localhost:54726}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 61e2799072b9428c4df94ca39d2a466b\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:408)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:276)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.sendKeys(RemoteWebElement.java:100)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:483)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:51)\r\n\tat com.sun.proxy.$Proxy17.sendKeys(Unknown Source)\r\n\tat helpdeskutility.pages.TicketCreation.ticketCreation(TicketCreation.java:176)\r\n\tat helpdeskutility.stepdefinition.HelpdeskStepDefinition.customer_creates_a_new_ticket_on_support_center_with_attachment(HelpdeskStepDefinition.java:51)\r\n\tat ✽.When Customer creates a new ticket on support center with attachment(src/test/java/helpdeskutility/features/helpdeskticket.feature:15)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "HelpdeskStepDefinition.agent_verifies_the_default_status_and_priority()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "HelpdeskStepDefinition.agent_replies_to_customer_using_canned_actions_and_checks_the_property_changes_of_status_and_priority()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 20,
  "name": "Agent deletes the status and priority then logs out of the portal",
  "description": "",
  "id": "helpdesk-ticket-workflow-of-happyfox-application;agent-deletes-the-status-and-priority-then-logs-out-of-the-portal",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 19,
      "name": "@DeleteStatusPriority"
    }
  ]
});
formatter.step({
  "line": 21,
  "name": "Agent deletes the created status and priority",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "Agent logs out of the portal",
  "keyword": "Then "
});
formatter.match({
  "location": "HelpdeskStepDefinition.agent_deletes_the_created_status_and_priority()"
});
formatter.result({
  "duration": 10424834000,
  "error_message": "org.openqa.selenium.json.JsonException: java.lang.reflect.InvocationTargetException\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027HP-PC\u0027, ip: \u0027192.168.0.106\u0027, os.name: \u0027Windows 8.1\u0027, os.arch: \u0027x86\u0027, os.version: \u00276.3\u0027, java.version: \u00271.8.0\u0027\nDriver info: driver.version: RemoteWebDriver\r\n\tat org.openqa.selenium.json.JsonOutput.convertUsingMethod(JsonOutput.java:315)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$new$14(JsonOutput.java:152)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$77/26799568.consume(Unknown Source)\r\n\tat org.openqa.selenium.json.JsonOutput$SafeBiConsumer.accept(JsonOutput.java:407)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:261)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$null$19(JsonOutput.java:165)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$99/8025644.accept(Unknown Source)\r\n\tat java.util.LinkedHashMap$LinkedValues.forEach(LinkedHashMap.java:600)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$new$20(JsonOutput.java:165)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$83/19461494.consume(Unknown Source)\r\n\tat org.openqa.selenium.json.JsonOutput$SafeBiConsumer.accept(JsonOutput.java:407)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:261)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$null$21(JsonOutput.java:174)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$95/25166952.accept(Unknown Source)\r\n\tat com.google.common.collect.SingletonImmutableBiMap.forEach(SingletonImmutableBiMap.java:65)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$new$22(JsonOutput.java:173)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$85/32914837.consume(Unknown Source)\r\n\tat org.openqa.selenium.json.JsonOutput$SafeBiConsumer.accept(JsonOutput.java:407)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:261)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:252)\r\n\tat org.openqa.selenium.json.Json.toJson(Json.java:42)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpCommandCodec.encode(AbstractHttpCommandCodec.java:227)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpCommandCodec.encode(AbstractHttpCommandCodec.java:117)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:152)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.perform(RemoteWebDriver.java:614)\r\n\tat org.openqa.selenium.interactions.Actions$BuiltAction.perform(Actions.java:640)\r\n\tat org.openqa.selenium.interactions.Actions.perform(Actions.java:596)\r\n\tat helpdeskutility.actionlib.ActionLib.ActionBuilderClick(ActionLib.java:83)\r\n\tat helpdeskutility.pages.DeletePrioStatus.deleteExistingPriority(DeletePrioStatus.java:69)\r\n\tat helpdeskutility.stepdefinition.HelpdeskStepDefinition.agent_deletes_the_created_status_and_priority(HelpdeskStepDefinition.java:68)\r\n\tat ✽.When Agent deletes the created status and priority(src/test/java/helpdeskutility/features/helpdeskticket.feature:21)\r\n\tSuppressed: org.openqa.selenium.json.JsonException: Attempting to close incomplete json stream\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027HP-PC\u0027, ip: \u0027192.168.0.106\u0027, os.name: \u0027Windows 8.1\u0027, os.arch: \u0027x86\u0027, os.version: \u00276.3\u0027, java.version: \u00271.8.0\u0027\nDriver info: driver.version: RemoteWebDriver\r\n\t\tat org.openqa.selenium.json.JsonOutput.close(JsonOutput.java:276)\r\n\t\tat org.openqa.selenium.json.Json.toJson(Json.java:44)\r\n\t\tat org.openqa.selenium.remote.http.AbstractHttpCommandCodec.encode(AbstractHttpCommandCodec.java:227)\r\n\t\tat org.openqa.selenium.remote.http.AbstractHttpCommandCodec.encode(AbstractHttpCommandCodec.java:117)\r\n\t\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:152)\r\n\t\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\t\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\t\tat org.openqa.selenium.remote.RemoteWebDriver.perform(RemoteWebDriver.java:614)\r\n\t\tat org.openqa.selenium.interactions.Actions$BuiltAction.perform(Actions.java:640)\r\n\t\tat org.openqa.selenium.interactions.Actions.perform(Actions.java:596)\r\n\t\tat helpdeskutility.actionlib.ActionLib.ActionBuilderClick(ActionLib.java:83)\r\n\t\tat helpdeskutility.pages.DeletePrioStatus.deleteExistingPriority(DeletePrioStatus.java:69)\r\n\t\tat helpdeskutility.stepdefinition.HelpdeskStepDefinition.agent_deletes_the_created_status_and_priority(HelpdeskStepDefinition.java:68)\r\n\t\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\t\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\t\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\t\tat java.lang.reflect.Method.invoke(Method.java:483)\r\n\t\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\t\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\t\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\t\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\t\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\t\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\t\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\t\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\t\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\t\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\t\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\t\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\t\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\t\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\t\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\t\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\t\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\t\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\t\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\t\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\t\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\t\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\t\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\t\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\t\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\t\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\t\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\t\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\t\tat org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:86)\r\n\t\tat org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)\r\n\t\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:538)\r\n\t\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:760)\r\n\t\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:460)\r\n\t\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:206)\r\nCaused by: java.lang.reflect.InvocationTargetException\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:483)\r\n\tat org.openqa.selenium.json.JsonOutput.convertUsingMethod(JsonOutput.java:311)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$new$14(JsonOutput.java:152)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$77/26799568.consume(Unknown Source)\r\n\tat org.openqa.selenium.json.JsonOutput$SafeBiConsumer.accept(JsonOutput.java:407)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:261)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$null$19(JsonOutput.java:165)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$99/8025644.accept(Unknown Source)\r\n\tat java.util.LinkedHashMap$LinkedValues.forEach(LinkedHashMap.java:600)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$new$20(JsonOutput.java:165)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$83/19461494.consume(Unknown Source)\r\n\tat org.openqa.selenium.json.JsonOutput$SafeBiConsumer.accept(JsonOutput.java:407)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:261)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$null$21(JsonOutput.java:174)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$95/25166952.accept(Unknown Source)\r\n\tat com.google.common.collect.SingletonImmutableBiMap.forEach(SingletonImmutableBiMap.java:65)\r\n\tat org.openqa.selenium.json.JsonOutput.lambda$new$22(JsonOutput.java:173)\r\n\tat org.openqa.selenium.json.JsonOutput$$Lambda$85/32914837.consume(Unknown Source)\r\n\tat org.openqa.selenium.json.JsonOutput$SafeBiConsumer.accept(JsonOutput.java:407)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:261)\r\n\tat org.openqa.selenium.json.JsonOutput.write(JsonOutput.java:252)\r\n\tat org.openqa.selenium.json.Json.toJson(Json.java:42)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpCommandCodec.encode(AbstractHttpCommandCodec.java:227)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpCommandCodec.encode(AbstractHttpCommandCodec.java:117)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:152)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.perform(RemoteWebDriver.java:614)\r\n\tat org.openqa.selenium.interactions.Actions$BuiltAction.perform(Actions.java:640)\r\n\tat org.openqa.selenium.interactions.Actions.perform(Actions.java:596)\r\n\tat helpdeskutility.actionlib.ActionLib.ActionBuilderClick(ActionLib.java:83)\r\n\tat helpdeskutility.pages.DeletePrioStatus.deleteExistingPriority(DeletePrioStatus.java:69)\r\n\tat helpdeskutility.stepdefinition.HelpdeskStepDefinition.agent_deletes_the_created_status_and_priority(HelpdeskStepDefinition.java:68)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:483)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:86)\r\n\tat org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:38)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:538)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:760)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:460)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:206)\r\nCaused by: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//a[text()\u003d\u0027Delete\u0027]\"}\n  (Session info: chrome\u003d84.0.4147.89)\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027HP-PC\u0027, ip: \u0027192.168.0.106\u0027, os.name: \u0027Windows 8.1\u0027, os.arch: \u0027x86\u0027, os.version: \u00276.3\u0027, java.version: \u00271.8.0\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 84.0.4147.89, chrome: {chromedriverVersion: 84.0.4147.30 (48b3e868b4cc0..., userDataDir: C:\\Users\\HP\\AppData\\Local\\T...}, goog:chromeOptions: {debuggerAddress: localhost:54726}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 61e2799072b9428c4df94ca39d2a466b\n*** Element info: {Using\u003dxpath, value\u003d//a[text()\u003d\u0027Delete\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:408)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:322)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:424)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:314)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy17.getWrappedElement(Unknown Source)\r\n\tat org.openqa.selenium.interactions.PointerInput$Origin.asArg(PointerInput.java:204)\r\n\tat org.openqa.selenium.interactions.PointerInput$Move.encode(PointerInput.java:155)\r\n\tat org.openqa.selenium.interactions.Sequence.encode(Sequence.java:75)\r\n\tat org.openqa.selenium.interactions.Sequence.toJson(Sequence.java:84)\r\n\t... 74 more\r\n",
  "status": "failed"
});
formatter.match({
  "location": "HelpdeskStepDefinition.agent_logs_out_of_the_portal()"
});
formatter.result({
  "status": "skipped"
});
});